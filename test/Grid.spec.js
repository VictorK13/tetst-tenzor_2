import Grid from '../src/Grid.js';
var expect = require('expect');

let X_axis = ["а", "б", "в", "г", "д", "е", "ж", "з", "и", "к"];
let Y_axis = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let gr = new Grid(X_axis, Y_axis);

var compareArrays = function (arr1, arr2) {
    let result;

    result = (
        arr1.length === arr2.length &&
        arr1.sort().every(function (value, index) {
            return value === arr2.sort()[index];
        })
    );

    return result;
};

describe("Grid", function () {
    describe(".getCoordinatesVector()", function () {

        it("returns array [a1, a2, a3, a4]", function () {
            let expectArr = ["а1", "а2", "а3", "а4"];
            let recivedArr = gr.getCoordinatesVector("а1", {
                y: 4
            });

            expect(recivedArr.length).toBe(expectArr.length);
            expect(recivedArr).toEqual(expect.arrayContaining(expectArr));
        });

        it("returns array [а2, б1, в1, г1]", function () {

            let expectArr = ["а1", "б1", "в1", "г1"];
            let recivedArr = gr.getCoordinatesVector("а1", {
                x: 4
            });

            expect(recivedArr.length).toBe(expectArr.length);
            expect(recivedArr).toEqual(expect.arrayContaining(expectArr));
        });

        it("returns array [и1, з1, ж1, е1]", function () {

            let expectArr = ["и1", "з1", "ж1", "е1"];
            let recivedArr = gr.getCoordinatesVector("и1", {
                x: -4
            });

            expect(recivedArr.length).toBe(expectArr.length);
            expect(recivedArr).toEqual(expect.arrayContaining(expectArr));
        });

        it("returns array [a10, a9, a8, a7]", function () {


            let expectArr = ["а10", "а9", "а8", "а7"];
            let recivedArr = gr.getCoordinatesVector("а10", {
                y: -4
            });

            expect(recivedArr.length).toBe(expectArr.length);
            expect(recivedArr).toEqual(expect.arrayContaining(expectArr));
        });

        it('returns array ["г8", "в8", "б8", "а8"]', function () {
            let expectArr = ["г8", "в8", "б8", "а8"];
            let recivedArr = gr.getCoordinatesVector("г8", {
                x: -4
            });

            expect(recivedArr.length).toBe(expectArr.length);
            expect(recivedArr).toEqual(expect.arrayContaining(expectArr));
        });

        it('Выбрасывает ошибку для параметров (a1, {y: -2}) ', function () {
            expect(() => {
                gr.getCoordinatesVector('а1', {
                    y: -2
                })
            }).toThrow();

        });

        it("it throws error if start coordinate is out of bounds", function () {

            let err, err1, err2, err3;

            try {
                err = gr.getCoordinatesVector("а10", {
                    y: 2
                });
            } catch (error) {
                err1 = true;
            }

            try {
                let err = gr.getCoordinatesVector("а1", {
                    y: -2
                });
            } catch (error) {
                err2 = true;
            }

            try {
                let err = gr.getCoordinatesVector("а1", {
                    x: -2
                });
            } catch (error) {
                err3 = true;
            }

            expect(err1 && err2 && err3).toBeTruthy();

        });

    });

    describe('.getNearestCoordinates()', function () {

        it('Возвращает список ближайших координат клетки a1', function () {

            let nearCoords = gr.getNearestCoordinatesFor('а1');
            expect(nearCoords).toEqual(['а2', 'б1', 'б2']);
        });

        it('Возвращает список ближайших координат для массива координат [б2, б3]',
            function () {
                let nearCoords = gr.getNearestCoordinatesFor(['б2', 'б3']);
                expect(nearCoords.length).toBe(10);
                expect(nearCoords).toEqual(["а1", "а2", "а3", "б1", "в1", "в2", "в3", "а4", "б4", "в4"]);
            });

    });

    describe('.getCoordSubSetForDirection()', function () {

        it('Возвращает подмножество координат для {x:4}: ',
            function () {
                let gr = new Grid(
                    ["а", "б", "в", "г", "д"], [1, 2]
                );

                let subset = gr.getSafeSubsetForFirstCoordinate({
                    x: 4
                });

                expect(subset.length).toBe(4);
                expect(subset).toEqual(expect.arrayContaining(['а1', 'б1', 'а2', 'б2']));
            }
        );

        it('Возвращает подмножество координат для {x:-3}: ',
            function () {
                let X_axis = ["а", "б", "в"];
                let Y_axis = [1, 2, 3];
                let gr = new Grid(X_axis, Y_axis);

                let subset = gr.getSafeSubsetForFirstCoordinate({
                    x: -3
                });

                expect(subset.length).toBe(3);
                expect(subset).toEqual(expect.arrayContaining(
                    ['в1', 'в2', 'в3', ]));

            }
        );

        it('Возвращает подмножество координат для {y:-2}: ',
            function () {
                let X_axis = ["а", "б", "в"];
                let Y_axis = [1, 2, 3];
                let gr = new Grid(X_axis, Y_axis);

                let subset = gr.getSafeSubsetForFirstCoordinate({
                    y: -2
                });

                expect(subset.length).toBe(6);

                expect(subset).toEqual(expect.not.arrayContaining(
                    ['а1', 'б1', 'в1']));

            }
        );
    });

    describe('.isCoordinatesOcupied', function () {
        let X_axis = ["а", "б", "в"];
        let Y_axis = [1, 2, 3];
        let Gr = new Grid(X_axis, Y_axis);

        Gr.markAsOccupied('а1');

        it('Возвращает true если заданные координаты заняты', function () {
            // Сама координата и координаты лежащие рядом должны быть помечены как занятые
            expect(Gr.isCoordinatesOcupied('а1')).toBe(true);
            expect(Gr.isCoordinatesOcupied('а2')).toBe(true);
            expect(Gr.isCoordinatesOcupied('б1')).toBe(true);
            expect(Gr.isCoordinatesOcupied('б2')).toBe(true);
        });

        it('Возвращает false если координата не занята', function () {
            expect(Gr.isCoordinatesOcupied('в1')).toBe(false);
            expect(Gr.isCoordinatesOcupied('в2')).toBe(false);
            expect(Gr.isCoordinatesOcupied('в3')).toBe(false);
            expect(Gr.isCoordinatesOcupied('а3')).toBe(false);
            expect(Gr.isCoordinatesOcupied('б3')).toBe(false);
        });
    });

});