import Field from '../src/Field.js';
import Ship from '../src/Ship.js';

var expect = require('expect');

describe('Field', function () {

    describe('Создание экземпляра класса Field', function () {
        it('Успешно создаёт экземпляр класса Field без входных параметров', function () {
            expect(new Field()).toBeTruthy();
        });

        it('Успешно создаёт экземпляр класса Field с входными параметрами', function () {
            expect(new Field(['b', 'c'], [1, 2])).toBeTruthy();
        });
    });

    describe('.addShip()', function () {

        it('Успешно добавляет корабль к полю', function () {
            let ship = new Ship(['b1'], '1deck-1');

            let field = new Field(['b', 'c'], [1, 2]);

            expect(field.addShip(ship)).toBeTruthy();
        });

        it('должен выдать ошибку при попытке добавить корабль на занятую ячейку', function () {
            let ship1 = new Ship(['b1'], '1deck-1');
            let ship2 = new Ship(['b1', 'b2'], '2deck-1');

            let field = new Field(['b', 'c'], [1, 2]);

            field.addShip(ship1);

            expect(() => {
                field.addShip(ship2, true)
            }).toThrow();
        });

        it('должен вернуть false при попытке добавить корабль на занятую ячейку', function () {
            let ship1 = new Ship(['b1'], '1deck-1');
            let ship2 = new Ship(['b1', 'b2'], '2deck-1');

            let field = new Field(['b', 'c'], [1, 2]);

            field.addShip(ship1);

            expect(field.addShip(ship2)).toBe(false);
        });



    });
});