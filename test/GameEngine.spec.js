import GameEngine from '../src/GameEngine.js';

var expect = require('expect');

let G = new GameEngine();

describe("GameEngine", function () {
    describe(".start()", function () {
        it('успешно создаёт объекты для игры', function () {
            expect(() => {
                G.start()
            }).not.toThrow();

            expect(G.field1).toEqual(expect.anything());
            expect(G.field2).toEqual(expect.anything());
            expect(G.player1).toEqual(expect.anything());
            expect(G.player2).toEqual(expect.anything());

            expect(G.field1.getShipsCounters().total).toBe(10);
            expect(G.field2.getShipsCounters().total).toBe(10);
        });

    });

});