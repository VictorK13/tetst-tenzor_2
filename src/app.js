require('./bootstrap.js');
import GameManager from './GameManager.js';

$(document).ready(() => {
    window.gameManager = new GameManager;
    window.gameManager.showWelcomeScreen();
});