export default class Ship {

    constructor(coordinates, name) {
        this.name = name;
        this.destroyed = false;
        this.decksCount = coordinates.length;
        this.coordinates = coordinates;
        this.hitLog = [];
    }

    registerHit(coordinate) {
        if (this.destroyed) {
            return false;
        }

        if (!this.isCoordinatesMine(coordinate)) {
            throw 'Не верные координаты. Удар мимо!';
        }

        this.hitLog.push(coordinate);
        this.decksCount--;
        if (this.decksCount === 0) {
            this.destroyed = true;
        }

        return true;
    }

    isDestroyed() {
        return this.destroyed;
    }

    isCoordinatesMine(coordinate) {
        ofCoordinate = function (el) {
            return el === coordinate;
        }

        return (this.coordinates.findIndex(ofCoordinate) !== -1) ? true : false;
    }

    getCoordinates() {
        return this.coordinates;
    }

}