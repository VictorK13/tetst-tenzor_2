import DecisionTree from "./DecisionTree.js";
import Memory from "./Memory.js";
export default class AI {
    constructor() {
        this.memory = {
            decisionLog: {},
            enemyShipDestroyed: []
        };

        this.decisionTree = new DecisionTree(this);
    }

    writeToMemory(dataKey, data) {
        this.memory["dataKey"] = data;
    }

    readFromMemory(dataKey) {
        return this.memory[dataKey];
    }

    wirteFeedback(feedback) {}

    /**
     *
     * @param {Field} enemyField
     */
    makeDecision(enemyField) {
        this.writeToMemory("enemyField", enemyField);

        let result = null;
        let inSearchOfPerfectMove = true;
        let node = this.decisionTree.start(input);

        while (inSearchOfPerfectMove) {
            if (node.isLeafNode()) {
                inSearchOfPerfectMove = false;
                result = node.activate();
            } else {
                node = node.activate();
            }
        }

        return result;
    }

    setLastShot(coord, result) {
        let lastShot = {
            coord,
            result
        };
        this.writeToMemory("lastShot", lastShot);
    }

    /**
     * @returns {object} {coord: 'а1', result: 'MISS'}
     */
    getLastShot() {
        return this.readFromMemory("lastShot");
    }

    /**
     *
     * Возвращает массив координат расположенных по соседству вертикально и горизонтально рядом с заданной координатой
     *
     * @param {string} coord
     * @returns {array}
     */
    getUpDownLeftRightCoords(coord) {
        let field = this.readFromMemory("enemyField");
        return field.getUpDownLeftRightCoords(coord);
    }

    /**
     *
     * Возвращает отрезок подбитых координат к которому принадлежит заданная координата
     *
     * @param {string} coord
     */
    getHitSegemnt(coord) {
        return this.readFromMemory("enemyField").getHitSegemnt();
    }

    getAxisFromCoords(coords) {}

    doesShipPressedToTheEdge(coords) {}

    shipPressedToTheLeftEdge(coords) {}

    shipPressedToTheTopEdge(coords) {}

    shipHasFreeCellsToTheTop(coords) {}

    shipHasFreeCellsToTheBottom(coords) {}

    shipHasFreeCellsToTheLeft(coords) {}

    shipHasFreeCellsToTheRight(coords) {}

    thisCoordArrayHasFreeCells(coords) {}

    takeFreeCoordsFromArr(coords) {}

    getCoordToTheRight(coords) {}

    getCoordToTheLeft(coords) {}

    getCoordAtTheTop(coords) {}

    getCoordAtTheBottom(coords) {}

    chooseRandomOne(coords) {}

    getRandomCoordFromField() {}
}