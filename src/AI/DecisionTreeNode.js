export default class DecisionTreeNode {

    constructor(options) {
        this.tree = options.tree;
        this.Fn = options.Fn;
        this.name = options.name;
        this.isLeafNode = options.isLeafNode;
        this.data = null;
    }

    activate(input) {
        // let context = this;
        let context = this.tree;
        return this.Fn.call(context, this.data, input);
    }

    isLeafNode() {
        return this.isLeafNode;
    }

    setData(data) {
        this.data = data;
    }

}