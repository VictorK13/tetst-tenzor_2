import Node from "./DecisionTreeNode.js";

export default class DecisionTree {
  constructor(AI) {
    this.AI = AI;
    this.nodes = this.buildNodes();
  }

  buildNodes() {
    let nodes = {};

    // Есть попадание?
    nodes.start = new Node({
      tree: this,
      Fn: () => {
        let result = this.Q_LastMoveWasAHit();
        node = result == true ? this.getNode("Y") : this.getNode("N");
        let lastShot = this.AI.getLastShot();
        if (node.name === "Y") {
          node.setData({
            coord: lastShot.coord
          });
        }
        return node;
      },
      name: "start",
      isLeafNode: false
    });

    // Подбито больше одной палубы?
    nodes.Y = new Node({
      tree: this,
      Fn: data => {
        let coords = this.Q_thereIsMoreThenOneDeckHitted(data.coord);
        let node = coords.length > 1 ? this.getNode("YY") : this.getNode("YN");
        if (node.name === "YY") {
          node.setData({
            coords
          });
        } else {
          node.setData({
            coord: data.coord
          });
        }
        return node;
      },
      name: "Y",
      isLeafNode: false
    });

    // Он расположен вдоль оси X?
    nodes.YY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_shipLiesAlongTheXaxis(data.coords);
        let node = result == true ? this.getNode("YYY") : this.getNode("YYN");
        node.setData({
          coords: data.coords
        });
        return node;
      },
      name: "YY",
      isLeafNode: false
    });

    // Он прижат к краю оси?
    nodes.YYY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_shipPressedToTheEdge(data.coords);
        let node = result == true ? this.getNode("YYYY") : this.getNode("YYYN");
        if (node.YYYY) {
          node.setData({
            coords: data.coords
          });
        }

        return node;
      },
      name: "YYY",
      isLeafNode: false
    });

    // Он прижат к левому краю?
    nodes.YYYY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_shipPressedToTheLeftEdge(data.coords);
        let node =
          result === true ? this.getNode("YYYYY") : this.getNode("YYYYN");

        node.setData({
          coords: data.coords
        });

        return node;
      },
      name: "YYYY",
      isLeafNode: false
    });

    // Выполнить: Выстрелить по правой координате.
    nodes.YYYYY = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireOnTheRightCoord(data.coords);
      },
      name: "YYYYY",
      isLeafNode: true
    });

    // НЕТ: Выполнить: Случайно выбрать координату из поля и Выстрелить.
    nodes.N = new Node({
      tree: this,
      Fn: data => {
        return this.Act_chooseOneCoordFromField();
      },
      name: "N",
      isLeafNode: true
    });

    // Среди соседних координат есть свободные?
    nodes.YN = new Node({
      tree: this,
      Fn: data => {
        let coords = this.Q_thereAreFreeCellToTheUpDownLeftRight(data.coords);
        let node = coords === true ? this.getNode("YNY") : this.getNode("YNN");
        if (node.name === "YNY") {
          node.setData({
            coords: coords
          });
        }
        return node;
      },
      name: "YN",
      isLeafNode: false
    });

    // Выполнить: Случайно выбрать координату из соседних и Выстрелить.
    nodes.YNY = new Node({
      tree: this,
      Fn: data => {
        return this.Act_chooseOneCoordFrom4AndFire(data.coords);
      },
      name: "YNY",
      isLeafNode: true
    });

    // Выполнить: Случайно выбрать координату из поля и Выстрелить.
    nodes.YNN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_chooseOneCoordFromField();
      },
      name: "YNN",
      isLeafNode: true
    });

    // Он прижат к краю оси?
    nodes.YYN = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_shipPressedToTheEdge(data.coords);
        let node = result == true ? this.getNode("YYNY") : this.getNode("YYNN");
        node.setData({
          coords: data.coords
        });

        return node;
      },
      name: "YYN",
      isLeafNode: false
    });
    // Он прижат к верхнему краю?
    nodes.YYNY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_shipPressedToTheTopEdge(data.coords);
        let node = result ? this.getNode("YYNYY") : this.getNode("YYNYN");
        node.setData({
          coords: data.coords
        });

        return node;
      },
      name: "YYNY",
      isLeafNode: false
    });

    // Выполнить: Выстрелить по нижней координате.
    nodes.YYNYY = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireAtTheBottomCoord(data.coords);
      },
      name: "YYNYY",
      isLeafNode: true
    });

    // Выполнить Выстрелить по верхней координате.
    nodes.YYNYN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireAtTheTopCoord(data.coords);
      },
      name: "YYNYN",
      isLeafNode: true
    });

    // Сверху от корабля есть свободные клетки?
    nodes.YYNN = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_thereAreFreeCellsToTheTopOfTheShip(data.coords);
        let node = result ? this.getNode("YYNNY") : this.getNode("YYNNN");
        node.setData({
          coords: data.coords
        });
        return node;
      },
      name: "YYNN",
      isLeafNode: false
    });
    // Снизу от корабля есть свободные клетки?
    nodes.YYNNY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_thereAreFreeCellsToTheBottomOfTheShip(data.coords);
        let node = result ? this.getNode("YYNNYY") : this.getNode("YYNNYN");
        node.setData({
          coords: data.coords
        });
        return node;
      },
      name: "YYNNY",
      isLeafNode: false
    });
    // Выполнить: Случайно выбрать одну координату из 2-ух и Выстрелить.
    nodes.YYNNYY = new Node({
      tree: this,
      Fn: data => {
        return this.Act_chooseOneCoordFrom2AndFire(data.coords);
      },
      name: "YYNNYY",
      isLeafNode: true
    });

    // Выполнить: Выстрелить по верхней координате.
    nodes.YYNNYN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireAtTheTopCoord(data.coords);
      },
      name: "YYNNYN",
      isLeafNode: true
    });

    // Выполнить: Выстрелить по нижней координате.
    nodes.YYNNN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireAtTheBottomCoord(data.coords);
      },
      name: "YYNNN",
      isLeafNode: true
    });
    // Слева от корабля есть свободные клетки?
    nodes.YYYN = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_thereAreFreeCellsToTheRightOfTheShip(data.coords);
        let node =
          result === true ? this.getNode("YYYNY") : this.getNode("YYYNN");
      },
      name: YYYN,
      isLeafNode: false
    });

    // Выполнить Выстрелить по левой координате.
    nodes.YYYYN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireOnTheLeftCoord(data.coords);
      },
      name: "YYYYN",
      isLeafNode: false
    });
    // Слева от корабля есть свободные клетки?
    nodes.YYYNY = new Node({
      tree: this,
      Fn: data => {
        let result = this.Q_thereAreFreeCellsToTheLeftOfTheShip(data.coords);
        let node =
          result === true ? this.getNode("YYYNYY") : this.getNode("YYYNYN");
        node.setData({
          coords: data.coords
        });
        return node;
      },
      name: YYYNY,
      isLeafNode: false
    });

    // Выподнить: Случайно выбрать одну координату из 2-ух и Выстрелить.
    nodes.YYYNYY = new Node({
      tree: this,
      Fn: data => {
        return this.Act_chooseOneCoordFrom2AndFire(data.coords);
      },
      name: "YYYNYY",
      isLeafNode: true
    });

    // Выполнить Выстрелить по левой координате.
    nodes.YYYNYN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireOnTheLeftCoord(data.coords);
      },
      name: "YYYNYN",
      isLeafNode: true
    });

    // Выполнить: Выстрелить по правой координате.
    nodes.YYYNN = new Node({
      tree: this,
      Fn: data => {
        return this.Act_fireOnTheRightCoord(data.coords);
      },
      name: "YYYNN",
      isLeafNode: true
    });

    return nodes;
  }

  getNode(nodeName) {
    return this.nodes["nodeName"];
  }

  start(input) {
    return this.nodes.start.activate(input);
  }

  /**
   * start
   * Есть попадание?
   *
   */
  Q_LastMoveWasAHit() {
    let lastShot = this.AI.getLastShot(); // lastShot = {coord: 'а1', result: 'MISS'}
    return lastShot.result.toLowerCase() === "hit" ? true : false;
  }

  /**
   * Y
   * Подбито больше одной палубы?
   *
   * @returns {array|false} еслм возле координаты есть подбитые клетки возвращает массив координат иначе false
   *
   */
  Q_thereIsMoreThenOneDeckHitted(coord) {
    return this.AI.getHitSegemnt(coord);
  }

  /**
   * Он расположен вдоль оси X?
   * @param {array} coords часть последовательности координат которая принадлежит кораблю
   */
  Q_shipLiesAlongTheXaxis(coords) {
    return this.AI.getAxisFromCoords(coords) === "X_axis";
  }

  /**
   * Он прижат к краю оси?
   * @param {array} coords часть последовательности координат которая принадлежит кораблю
   */
  Q_shipPressedToTheEdge(coords) {
    return this.AI.doesShipPressedToTheEdge(coords);
  }
  /**
   * Он прижат к левому краю?
   */
  Q_shipPressedToTheLeftEdge(coords) {
    return this.AI.shipPressedToTheLeftEdge(coords);
  }

  /**
   * Он прижат к верхнему краю?
   * @param {array} coords
   */
  Q_shipPressedToTheTopEdge(coords) {
    return this.AI.shipPressedToTheTopEdge(coords);
  }

  /**
   * Сверху от корабля есть свободные клетки?
   * @param {array} coords
   */
  Q_thereAreFreeCellsToTheTopOfTheShip(coords) {
    return this.AI.shipHasFreeCellsToTheTop(coords);
  }

  /**
   * Снизу от корабля есть свободные клетки?
   * @param {array} coords
   */
  Q_thereAreFreeCellsToTheBottomOfTheShip(coords) {
    return this.AI.shipHasFreeCellsToTheBottom(coords);
  }

  /**
   * Слева от корабля есть свободные клетки?
   */
  Q_thereAreFreeCellsToTheLeftOfTheShip(coords) {
    return this.AI.shipHasFreeCellsToTheLeft(coords);
  }

  /**
   * Справа от корабля есть свободные клетки?
   */
  Q_thereAreFreeCellsToTheRightOfTheShip(coords) {
    return this.AI.shipHasFreeCellsToTheRight(coords);
  }

  /**
   * Среди соседних координат есть свободные?
   * @param {string} coord
   */
  Q_thereAreFreeCellToTheUpDownLeftRight(coord) {
    let coords = this.AI.getUpDownLeftRightCoords(coord);
    let result = this.AI.thisCoordArrayHasFreeCells(coords);
    let freeCoords = this.AI.takeFreeCoordsFromArr(coords);
    return result ? freeCoords : false;
  }

  /**
   * Выполнить: Выстрелить по правой координате.
   *
   * @param {array} coords последовательность координат относительно которой нужно отобрать координату правее
   */
  Act_fireOnTheRightCoord(coords) {
    return this.AI.getCoordToTheRight(coords);
  }

  /**
   * Выполнить Выстрелить по левой координате.
   *
   * @param {array} coords последовательность координат относительно которой нужно отобрать координату левее
   */
  Act_fireOnTheLeftCoord(coords) {
    return this.AI.getCoordToTheLeft(coords);
  }

  /**
   * Выполнить Выстрелить по верхней координате.
   *
   * @param {array} coords последовательность координат относительно которой нужно отобрать координату левее
   */
  Act_fireAtTheTopCoord(coords) {
    return this.AI.getCoordAtTheTop(coords);
  }

  /**
   * Выполнить Выстрелить по нижней координате.
   *
   * @param {array} coords последовательность координат относительно которой нужно отобрать координату левее
   */
  Act_fireAtTheBottomCoord(coords) {
    return this.AI.getCoordAtTheBottom(coords);
  }

  /**
   * Выполнить: Случайно выбрать одну координату из 2-ух и Выстрелить.
   * @param {array} coords Две координаты из которых случайным образом нужно выбрать одну
   */
  Act_chooseOneCoordFrom2AndFire(coords) {
    return this.AI.chooseRandomOne(coords);
  }

  /**
   * Выполнить: Случайно выбрать координату из соседних и Выстрелить.
   */
  Act_chooseOneCoordFrom4AndFire(coords) {
    return this.AI.chooseRandomOne(coords);
  }

  /**
   * Выполнить: Случайно выбрать координату из поля и Выстрелить.
   */
  Act_chooseOneCoordFromField() {
    return this.AI.getRandomCoordFromField();
  }
}
