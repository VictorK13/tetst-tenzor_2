export default class Renderer {

    constructor(el, game) {
        this.el = el;
        this.game = game;
    }

    render(renderable) {
        // TODO: Написать render метод

    }

    // TODO: Перенести методы ниже в отдельные Renderable классы
    showWelocomeForm() {
        $(this.el).add(this.createWelcomeForm());
    }


    addClickHandler(selector, cb) {
        $(selector).click(cb);
    }

    showPlayground() {

    }

    showWinnerBanner(player) {

    }

    removeField(field) {
        $('#' + field.getName()).remove();
    }

    renderField(field) {
        let fieldEl = this.drawField(field);
        $(this.el).add(fieldEl);
    }

    drawField(field) {
        let fieldEl = $("div").addClass('field').attr('id', field.getName());
        let rowLegendEl = $("div").addClass('rowLegend');
        let colLegendEl = $("div").addClass('colLegend');
        let cellGridEl = $('div').addClass('cells-grid');

        let cells = field.getCells();
        let rowCount = field.getRowCount();

        for (let i = 1; i <= rowCount; i++) {
            rowEl = $('div').addClass('row').attr('id', field.getName() + '-row' + i);
            field.getCellsFromRow(i).forEach(cell => {
                let cellEl = $('span').addClass('cell').attr('id', field.getName() + '-' + cell.getAddress());
                rowEl.add(cellEl);
            });
            cellGridEl.add(rowEl);
        }
        fieldEl.add(rowLegendEl);
        fieldEl.add(colLegendEl);
        fieldEl.add(cellGridEl);

        return fieldEl;
    }

    paintCell(cell, color) {
        let selector = '#' + cell.getAddress();
        $(selector).setClass(color);
    }

    paintCellHitState(cell) {
        this.paintCell(cell, "cell-state-hit");
    }

    paintCellMissState(cell) {
        this.paintCell(cell, "cell-state-miss");
    }

    paintCellMarkedState(cell) {
        this.paintCell(cell, "cell-state-marked");
    }

    handleUserInput(ev) {
        let actionType = ev.actionType;
        switch (actionType) {

            case "leftClick":
                result = this.game.handleTurn(ev);
                if (result === 'hit') {
                    this.paintCellHitState(ev.cell);
                } else if (result === 'miss') {
                    this.paintCellMissState(ev.cell);
                }
                if (this.game.isOver()) {
                    this.showWinnerBanner(this.game.getWinner());
                } else {
                    this.switchPlayerTurn();
                }
                break;

            case "rightClick":
                this.paintCellMarked(cell);
                break;
        }
    }
}