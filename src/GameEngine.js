import Field from './Field.js';
import Shipyard from './Shipyard.js';
import Player from './Player.js';
import AI from './AI/AI.js';

export default class GameEngine {


  constructor() {
    this.playerName = 'player1';
    this.field1 = null;
    this.field2 = null;
    this.player1 = null;
    this.player2 = null;
    this.currentTurn = null;
  }

  setPlayerName(name) {
    this.playerName = name;
  }

  start() {
    this.field1 = new Field();
    this.field2 = new Field();

    this.player1 = new Player(this.field1, this.playerName);
    this.player2 = new Player(this.field2, 'Компьютер');
    // this.player2.addAI(new AI());

    this.generateShipsFor(this.player1);
    this.generateShipsFor(this.player2);

    this.currentTurn = this.player1;

    return true;
  }

  generateShipsFor(player) {
    console.info('НАЧИНАЮ ГЕНЕРИРОВАТЬ КОРАБЛИ ДЛЯ ИГРОКА', player.getName());

    let field = player.getField();
    let shipYard = new Shipyard(field.getGrid());
    let contract = this.getContract();
    const LOOP_THRESHOLD = 1000;

    contract.forEach((shipOrder) => {
      for (let i = 1; i <= shipOrder.amount; i++) {
        let shipAdded = false;
        let forceBreak = false;
        let iter = 1;
        while (!shipAdded || forceBreak) {
          try {
            let ship = shipYard.build(shipOrder.deckCount, shipOrder.deckCount + 'deck-' + i);
            shipAdded = field.addShip(ship);
            if (shipAdded) {
              console.info('Корабль добавился ', {
                ship
              });
            } else {
              console.info('Корабль не добавился', {
                ship,
                iter
              });
            }
          } catch (er) {
            console.error('Корабль забракован Из-за ошибки: ', er);
            throw "В процессе рандомного размещения кораблей произошла ошибка";
          }

          forceBreak = (++iter > LOOP_THRESHOLD) ? true : false;
          if (forceBreak) {
            console.error('Зациклился!!! Принудительно завершаю работу');
          }

        }

      }

    })
  }

  getContract() {
    return [{
        deckCount: 4,
        amount: 1
      },
      {
        deckCount: 3,
        amount: 2
      },
      {
        deckCount: 2,
        amount: 3
      },
      {
        deckCount: 1,
        amount: 4
      },
    ];

  }

  handleMove(coord) {
    let player = this.getCurrentTurnPlayer();
    let opponentsField = this.getOpponentOfThe(player).getField();
    let result = opponentsField.reciveEnemyFire(coord);

    if (opponentsField.noMoreShipsLeft()) {
      result.winner = player;
    }

    return result;
  }

  getFields() {
    return {
      'field1': this.field1,
      'field2': this.field2
    };
  }

  stop() {
    //очистить всё что объявили в конструкторе
  }

  restart() {
    this.stop();
    this.start();
  }

  /**
   * Возращает игрока чей ход должен быть следующим
   * @returns {Player}
   */
  nextTurn() {
    switch (this.currentTurn) {
      case this.player1:
        this.currentTurn = this.player2;
        break;
      case this.player2:
        this.currentTurn = this.player1;
        break;
    }

    return this.currentTurn;
  }

  /**
   * Возвращает клон поля сопреника в котором затёрты все корабли но есть 
   * журнал всех попаданий и промахов
   * 
   * @param {Player} player 
   */
  getEnemyField(player) {

  }

}