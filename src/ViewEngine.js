export default class ViewEngine {

    constructor(containerSelector) {
        this.containerSelector = containerSelector;
        console.log(this.containerSelector);
        this.handlers = {
            Start: null,
            PlayerMove: null,
            EndGame: null,
            RestartGame: null,
        };
        this.currentScene = '';
        this.sceneSelectors = {
            welcome: '#welcome',
            battlefield: '#battlefield',
            winner: '#winner'
        }
    }


    showWelcomeScreen() {
        let template = `
            <div class="welcome-scene">
                <form>
                    <div>
                        <h1>Добро пожаловать</h1>
                    </div>
                    <div>
                        <input id="player-name" placeholder="Представтесь пожалуйста" type="text">
                    </div>
                    <div>
                        <button id="btn-start" class="btn blue btn-start" type="button">Играть</button>
                    </div>
                </form>
            </div>
        `;

        this.cleanScene();
        this.currentScene = this.sceneSelectors.welcome;

        $(this.containerSelector).append(template);
        $('#btn-start').click((e) => {
            return this.handlers.Start();
        });
    }

    showLoadingSpinner() {

        let template = `<div class="spinner"></div>`;
        this.cleanScene();
        this.currentScene = '.spinner';

        $(this.containerSelector).append(template);
    }

    showBattleField(fields) {
        let template = `
            <div id='battlefield'>
                <div id="field1"></div>
                <div id="field3"></div>
            </div>
        `;

        this.cleanScene();
        this.currentScene = this.sceneSelectors.battlefield;
        return $(this.containerSelector).append(template);
    }

    cleanScene() {
        if (this.currentScene && $(this.containerSelector).is(this.currentScene)) {
            $(this.currentScene).remove();
            this.currentScene = '';
        }

        return $(this.containerSelector);
    }

    setStartHandler(fn) {
        this.handlers.Start = fn;
    }

    setPlayerMoveHandler(fn) {
        this.handlers.PlayerMove = fn;
    }

    setEndGameHandler(fn) {
        this.handlers.EndGame = fn;
    }

    setRestartGameHandler(fn) {
        this.handlers.RestartGame = fn;
    }

    readPlayerName() {
        return $('#playerName').val();
    }

    updateBattleField(result) {

    }

    anouncePlayersTurn(player) {

    }

    unmountBattleField() {
        $("#battlefield").remove();
    }

}