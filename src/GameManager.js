import GameEngine from './GameEngine.js';
import ViewEngine from './ViewEngine.js';

export default class GameManager {

    constructor() {
        this.engine = new GameEngine();
        this.view = new ViewEngine('.container');

        this.setUpHandlers();
    }
    setUpHandlers() {
        this.view.setStartHandler((e) => {
            this.startGame();
        });
        this.view.setPlayerMoveHandler((playersMove) => {
            this.handlePlayerMove(playersMove);
        });
        this.view.setEndGameHandler((e) => {
            this.endGame();
        });
        this.view.setRestartGameHandler((e) => {
            this.restartGame();
        });
    }

    startGame() {
        this.engine.setPlayerName(this.view.readPlayerName());
        this.engine.start();
        this.showBattleField();
    }

    handlePlayerMove(playerMove) {
        let result = this.engine.handleMove(playerMove);
        this.view.updateBattleField(result);

        if (result.hasOwnProperty('winner')) {
            this.engine.stop();
            this.unmountBattleField();
            this.showWinnerScreen(result.winner);
            return;
        }

        let nextPlayer = this.engine.nextTurn();

        if (nextPlayer.hasAI()) {
            let field = this.engine.getEnemyField(nextPlayer);
            let move = nextPlayer.makeMove(field);
            this.handlePlayerMove(move); // 😯!!! РЕКУРСИЯ !!!😯
        } else {
            this.view.anouncePlayersTurn(nextPlayer);
            return;
        }

        throw 'RECURSION BREAKER: Что-то пошло не так! ';
    }

    endGame() {
        this.engine.stop(); // удаляет игровые объекты: Игроков, поля, корабли
        this.view.unmountBattleField(); // удаляет игровое поле
        this.view.showWelcomeScreen(); // показывает экран приветствия
    }

    restartGame() {
        this.engine.restart(); // очищает поля от кораблей и наполняет их по новой
        this.view.unmountBattleField(); // удаляет игровое поле
        this.view.showBattleField(this.engine.getFields()); // рисует новое ировое поле
    }

    showWelcomeScreen() {
        this.view.showWelcomeScreen();
    }

    showBattleField() {
        this.view.showBattleField(this.engine.getFields());
    }

}