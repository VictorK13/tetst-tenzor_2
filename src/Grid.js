import {
    getRandomInt,
    arr_diff
} from "./lib.js";

export default class Grid {
    constructor(x_axis, y_axis) {
        this.x_axis = {};
        this.y_axis = {};

        this.setXAxis(x_axis);
        this.setYAxis(y_axis);

        this.occupiedCoordinates = [];
        this.coordSplitRegEx = /([^0-9]+)([0-9]+)/;

        this.coordinateSet = [];
    }

    setXAxis(axis) {
        this.setAxis("x_axis", axis);
    }
    setYAxis(axis) {
        this.setAxis("y_axis", axis);
    }

    setAxis(name, axis) {
        axis.forEach((val, index) => {
            this[name][index + 1] = val;
        });
    }

    /**
     * Метод возвращает вектор координат начиная со стартовой координаты
     * в направлении задданном в аргументе directions
     *
     * @param string startCoord координата должна принадлежать заданной сетке координат
     * @param object direction {x: -4} - означает отложить 4 клетки по оси x слева от стартовой координаты
     */
    getCoordinatesVector(startCoord, direction) {
        let i, axis, startI, diff, startVal;
        let vec = [];

        let match = this.coordSplitRegEx.exec(startCoord);
        let x_start = match[1];
        let y_start = match[2];
        let startCoordIndexes;

        if (direction.hasOwnProperty("x")) {
            axis = "x_axis";
            i = direction.x;
            startI = this._getCoordIndexByVal(axis, x_start); // находим индекс координаты начала вектора
        } else {
            axis = "y_axis";
            i = direction.y;
            startI = this._getCoordIndexByVal(axis, y_start); // находим индекс координаты начала вектора
        }
        diff = i > 0 ? -1 : 1;
        i = i + diff; // так как startCoord уже входит в вектор нжуно найти только оставшиеся

        if (startI === -1) {
            throw "Grid: Значение стартовой координаты не принадлежит заданной сетке: " +
                startCoord;
        }

        if (this.checkIfVectorOutsideOfBounds(axis, startI, i)) {
            throw "Grid: Вектор координат выходит за пределы заданной сетки";
        }

        ////// Всё готово для создания вектора //////
        while (i !== 0) {
            let coord = this[axis][startI + i];
            coord = axis === "x_axis" ? coord + y_start : x_start + coord;
            vec.push(coord);
            i = i + diff;
        }

        vec.push(startCoord);

        return vec.sort();
    }

    /**
     * Возвращает индекс оси координат значения
     *
     * @param string axis
     * @param string coordVal
     */
    _getCoordIndexByVal(axis, coordVal) {
        let startI;

        Object.keys(this[axis]).find(key => {
            if (this[axis][key] == coordVal) {
                startI = parseInt(key);
            }
        });

        return startI;
    }

    getRandomCoordinate(coordSet) {
        if (typeof coordSet === "undefined") {
            coordSet = this.getCoordinateSet();
        }

        let randomI = getRandomInt(0, coordSet.length);

        return coordSet[randomI];
    }

    getNonOccupiedCoords() {
        return arr_diff(this.getCoordinateSet(), this.occupiedCoordinates);
    }

    getCoordinateSet() {
        if (this.coordinateSet.length === 0) {
            for (let i in this.x_axis) {
                for (let j in this.y_axis) {
                    this.coordinateSet.push(
                        String.prototype.concat(this.x_axis[i], this.y_axis[j])
                    );
                }
            }
        }

        return this.coordinateSet;
    }

    /**
     * TODO: написать тесты
     *
     * Метод возвращает подмножество координат из сетки подходящих для размещения
     * стартовой координаты вектора исключая те в которых вектор будет за пределами
     * сетки
     *
     * Полученное подмножество позволит уменьшить количество бракованных итераций
     * при рандомной расстановке кораблей убрав заранее известные не легальные координаты
     * коробля.
     *
     * Например для 4х палубинка нельзя отложить координаты к северу от а3 так как он
     * окажется за пределами сетки
     *
     * @param object direction {x: -4} или {y: 3}
     */
    getSafeSubsetForFirstCoordinate(direction) {
        let axis, axisLabel, coordinates;
        let safeAxisZone = [];
        let start, i, step, directionValue, axisLength;

        if (direction.hasOwnProperty("x")) {
            axis = this.x_axis;
            axisLabel = "x";
        } else {
            axis = this.y_axis;
            axisLabel = "y";
        }
        directionValue = direction[axisLabel];

        axisLength = Object.keys(axis).length;
        const offset = axisLength - Math.abs(directionValue);

        if (directionValue > 0) {
            start = i = 1;
            step = +1;
        } else {
            start = i = axisLength;
            step = -1;
        }

        while (Math.abs(start - i) <= offset) {
            safeAxisZone.push(axis[i]);
            i = i + step;
        }

        coordinates = this.getCoordinateSet();

        // Отфильтровать общий массив координат оставив только те что входят в safeAxisZone
        let safeCoordinates = coordinates.filter(
            coord => {
                let xy = this.getXY(coord);
                let found = safeAxisZone.findIndex((el) => el == xy[axisLabel]);
                return found > -1;
            }
        );

        return safeCoordinates;
    }

    _getXaxisLength() {
        return Object.keys(this.x_axis).length;
    }
    _getYaxisLength() {
        return Object.keys(this.y_axis).length;
    }

    checkIfVectorOutsideOfBounds(axis, start, diff) {
        if (undefined === this[axis][start + diff]) {
            return true;
        }
        return false;
    }

    // TODO: Исправить: если корабли не пересекаюстя но их смежные клетки пересекаются возвращает true
    isCoordinatesOcupied(coordinates) {
        let isOccupied = false;
        coordinates = (!Array.isArray(coordinates)) ? [coordinates] : coordinates;

        coordinates
            .forEach(coord => {
                let found = this.occupiedCoordinates.findIndex(el => el === coord);

                if (found > -1) {
                    isOccupied = true;
                }
            });

        return isOccupied;
    }

    markAsOccupied(coordinates) {
        coordinates = (!Array.isArray(coordinates)) ? [coordinates] : coordinates;

        this.getNearestCoordinatesFor(coordinates)
            .concat(coordinates)
            .forEach(coord => this.addToOccupied(coord));

        return true;
    }

    addToOccupied(coord) {
        this.occupiedCoordinates.push(coord);
    }

    getXY(coord) {
        let match = this.coordSplitRegEx.exec(coord);

        return {
            x: match[1],
            y: match[2]
        };
    }

    /**
     * Возвращает соседние координаты 
     * 
     * @param {string} coord 
     * @param {boolean} excludeDiagonalCoords Если true вернёт 4 координаты без диагональных
     * @returns {array}
     */
    getCoordNeighbors(coord, excludeDiagonalCoords) {
        excludeDiagonalCoords = (excludeDiagonalCoords === undefined) ? false : excludeDiagonalCoords;

        let neighbors = [];
        let point = this.getXY(coord);
        let startI = this._getCoordIndexByVal("x_axis", point.x);
        let startJ = this._getCoordIndexByVal("y_axis", point.y);

        if (startI === -1 || startJ === -1) {
            throw "Grid: Координата не принадлежит заданной сетке: " + coord;
        }

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (i === 0 && j === 0) {
                    continue;
                }
                let diagonalCoord = (Math.abs(i) + Math.abs(j)) > 1;
                if (excludeDiagonalCoords === true && diagonalCoord) {
                    continue;
                }
                let x = this.x_axis[startI + i];
                let y = this.y_axis[startJ + j];
                if (typeof x === "undefined" || typeof y === "undefined") {
                    continue;
                }
                // на случай если обе координаты будут цифрами мы не используем тут операцию +
                let xy = String.prototype.concat(x, y);

                neighbors.push(xy);
            }
        }

        return neighbors;
    }

    getNearestCoordinatesFor(coordinates) {
        if (!Array.isArray(coordinates)) {
            coordinates = [coordinates];
        }

        let neighborsArr = {};
        let stack = coordinates.slice();

        while (stack.length > 0) {
            let coord = stack.shift();
            let coordNeighbors = this.getCoordNeighbors(coord);
            coordNeighbors.forEach(neighbor => {
                neighborsArr[neighbor] = neighbor;
            });
        }
        coordinates.forEach(coord => {
            neighborsArr[coord] = undefined;
        });

        return Object.values(neighborsArr).filter(el => el !== undefined);
    }
}