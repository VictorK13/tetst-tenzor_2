import Grid from './Grid.js';

export default class Field {

    constructor(letters, numbers) {

        this.letters = (undefined !== letters) ?
            letters : ['а', "б", "в", "г", "д", "е", "ж", "з", "и", "к"];

        this.numbers = (undefined !== numbers) ?
            numbers : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        this.grid = new Grid(this.letters, this.numbers);

        this.ships = {};
    }

    addShip(ship, throwError) {

        if (this.grid.isCoordinatesOcupied(ship.getCoordinates())) {

            if (throwError === true) {
                throw "Нельзя разместить корабль в этих клетках: " + ship.coordinates.join();
            } else {
                return false;
            }
        }

        this.ships[ship.name] = ship;
        this.grid.markAsOccupied(ship.getCoordinates());

        return true;
    }

    getGrid() {
        return this.grid;
    }

    getShips() {
        return this.ships;
    }

    getShipsCounters() {
        let counters = {
            total: 0,
            operational: 0,
            destroyed: 0
        };

        for (let shipName in this.ships) {
            counters.total++;

            if (this.ships[shipName].isDestroyed()) {
                counters.destroyed++;
            } else {
                counters.operational++;
            }
        }

        return counters;
    }

    reciveEnemyFire(coord) {

    }

    getUpDownLeftRightCoords(coord) {
        return this.Grid.getCoordNeighbors(coord, true);
    }

}