import {
    getRandomInt
} from './lib.js';
import Ship from './Ship.js';

export default class Shipyard {


    constructor(grid) {
        this.grid = grid;

    }

    getRandomName(deckCount) {
        return deckCount + 'deck-' + Math.floor(Math.random() * 1000); // 4deck-999
    }

    getRandomDirection(deckCount) {
        let randVal = getRandomInt(1, 4);
        switch (randVal) {
            case 1:
                return {
                    x: 1 * deckCount
                };
            case 2:

                return {
                    x: -1 * deckCount
                };
            case 3:

                return {
                    y: 1 * deckCount
                };
            case 4:

                return {
                    y: -1 * deckCount
                };
        }

    }

    /**
     * Метод рандомно выбирает стартовую координату и рандомно выбирает направление
     * затем прокладывает отрезок координат равный длинне корабля
     * Если клетки не заняты создаёт корабль
     * 
     * @param integer deckCount 
     * @param string name 
     */
    build(deckCount, name) {
        if (typeof name === undefined) {
            name = this.getRandomName(deckCount);
        }

        console.info('Shipyard.build: Выбираю координаты для', name);
        let coordinates = false;
        let iter = 0;
        // const coordSet = this.grid.();
        do {
            let direction = this.getRandomDirection(deckCount);
            const coordSubSet = this.grid.getSafeSubsetForFirstCoordinate(direction);
            let startCoord = this.grid.getRandomCoordinate(coordSubSet);

            try {

                coordinates = this.grid.getCoordinatesVector(startCoord, direction);
            } catch (er) {
                console.info('Shipyard.build: Координаты не пригодны: ', {
                    coordinates,
                    er
                });
            }

            console.info('Coordinates: ', coordinates);
        } while (coordinates === false);

        console.info('Shipyard.build: Найдены подходязие координаты: ', coordinates);

        let ship = new Ship(coordinates, name);

        return ship;
    }

}