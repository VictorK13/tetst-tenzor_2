export default class Player {

    constructor(field, name) {
        this.field = field;
        this.name = name;
        this.AI = null;
    }

    getField() {
        return this.field;
    }

    getName() {
        return this.name;
    }

    hasAI() {
        return this.AI !== null;
    }

    addAI(AI) {
        this.AI = AI;
    }

    makeMove(enemyField) {
        if (this.AI === null) {
            throw 'Player.makeMove(): Я не могу, нужен мозг. :`(';
        }
        return this.AI.makeDecision(enemyField);
    }

}